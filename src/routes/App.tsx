import React from 'react';
import './App.css';
import TopNavigation from "../components/ui/top-navigation/TopNavigation";
import { Outlet } from "react-router-dom";

export default function App() {
  return (
    <div className="App">
      <TopNavigation/>
      <Outlet/>
    </div>
  );
}
