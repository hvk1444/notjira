import React from 'react';
import './MainPage.css';
import Dashboard from "../../components/ui/dashboards/dashboard/Dashboard";

const tasksDashboardProps = {
  title: 'Tasks',
  headerValues: ['Key','Summary','Assignee','Created'],
  bodyValues: [
    ['NTJ-1','Create mockup main page','Maciej Zarębski','2024-06-14'],
    ['NTJ-2','Create \'Tasks\' view','Maciej Zarębski','2024-06-19'],
    ['NTJ-3','Create \'Boards\' view',null,'2024-06-19'],
  ],
};
const boardsDashboardProps = {
  title: 'Boards',
  headerValues: ['WIP'],
  bodyValues: [['WIP']]
};

export default function MainPage() {
  return (
    <main className="Main-page">
      <header className="main-page-header">
        <h2>MAIN PAGE</h2>
      </header>
      <div className="dashboards-container">
        <Dashboard props={tasksDashboardProps}/>
        <Dashboard props={boardsDashboardProps}/>
      </div>
    </main>
  );
}
