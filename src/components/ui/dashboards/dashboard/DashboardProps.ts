export interface DashboardProps {
  title: string;
  headerValues: string[]
  bodyValues: (string | null)[][];
}