import "./Dashboard.css";
import {DashboardProps} from "./DashboardProps";
import React from "react";

export default function Dashboard({props}: { props: DashboardProps}) {
  //TODO add interactivity with useState
  const header = (<>
      <tr>
        {props.headerValues.map(headerValue => (<th>{headerValue}</th>))}
      </tr>
    </>);
  const rows = (<>
    {props.bodyValues.map(row => (
      <tr>
        {row.map(value => (<td>{value ? value : '-'}</td>))}
      </tr>)
    )}
    </>);

  return (
    <article className="Dashboard">
      <header className="dashboard-header">{props.title}</header>
      <table className="dashboard-table">
        <thead className="dashboard-thead">
          {header}
        </thead>
        <tbody className="dashboard-table-body">
          {rows}
        </tbody>
      </table>
    </article>
  );
}
