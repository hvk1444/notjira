import React from 'react';
import './TopNavigation.css';
import {NavLink} from "react-router-dom";

export default function TopNavigation() {
  return (
    <header className="Header-bar">
      <NavLink
        to="main"
        className={({ isActive}) => isActive ? "active-nav-button" : "nav-button"}>
        NOTJIRA
      </NavLink>
      <NavLink
        to="tasks"
        className={({ isActive}) => isActive ? "active-nav-button" : "nav-button"}>
        Tasks
      </NavLink>
      <NavLink
        to="boards"
        className={({ isActive}) => isActive ? "active-nav-button" : "nav-button"}>
        Boards
      </NavLink>
    </header>
  );
}
