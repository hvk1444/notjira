import {createBrowserRouter, Navigate} from "react-router-dom";
import App from "../../routes/App";
import ErrorPage from "../../Error-page";
import MainPage from "../../routes/main-page/MainPage";
import BoardsPage from "../../routes/boards-page/BoardsPage";
import TasksPage from "../../routes/tasks-page/TasksPage";
import React from "react";

export default function getDefaultRouter() {
  return createBrowserRouter([
    {
      path: "/",
      element: <App/>,
      errorElement: <ErrorPage/>,
      children: [
        {
          index: true,
          element: <Navigate to="/main" replace />
        },
        {
          path: "/main",
          element: <MainPage/>
        },
        {
          path: "/boards",
          element: <BoardsPage/>
        },
        {
          path: "/tasks",
          element: <TasksPage/>
        },
      ]
    },
  ]);
}